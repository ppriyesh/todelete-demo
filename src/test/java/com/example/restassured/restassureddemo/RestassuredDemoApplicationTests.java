package com.example.restassured.restassureddemo;

import static org.mockito.ArgumentMatchers.contains;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import static io.restassured.matcher.RestAssuredMatchers.*;

@SpringBootTest
class RestassuredDemoApplicationTests {

	private static final Logger logger = LoggerFactory.getLogger(RestassuredDemoApplicationTests.class);

	@Test
	void contextLoads() {
		logger.info("Hello Tests");
	}

	@Test
	void testGetListOfUsersSinglePage() {
		logger.info("Starting test case to get all users in single page");
		given()
			.when()
				.get("https://reqres.in/api/users?page=2")
			.then()
				.statusCode(200)
			.and()
				.body("page", equalTo(2));
	}

}
